# A React Task
* In this task, we will create a login form and apply styling to it. In the project we have a .css file which we have to transform to a **CSS module**.
## Objective
* Checkout the dev branch.
* Create a **LoginForm** component that has the same name as the styles.css file.
* In the LoginForm component, we have to create a form, labels and inputs for **username** and **password** fields and **Submit button** of type **submit**.
* Convert the .css file to a CSS module and apply the styles to the newly created **LoginForm** component.
## Requirements
The project starts with **npm run start**

## Gotchas
* Below  you can see the desired styling of the login form.
![](https://boomcdn.fra1.digitaloceanspaces.com/79659652a4c684500665138cf8c841b2.png)
